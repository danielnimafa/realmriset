package com.kipacraft.riset.realmriset.event

class EditEvent {
    var index: Int = 0
    var fieldName: String = ""
    var value: String = ""
    var position: Int = 0
}