package com.kipacraft.riset.realmriset

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import io.realm.Realm

class LinearActivity : AppCompatActivity() {

    lateinit var realm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        realm.close()
        super.onDestroy()
    }
}