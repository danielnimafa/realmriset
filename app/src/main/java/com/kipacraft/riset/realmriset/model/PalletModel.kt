package com.kipacraft.riset.realmriset.model

data class PalletModel(var index: Int = 0,
                       var code: String = "",
                       var length: String = "",
                       var weight: String = "",
                       var id: String = "",
                       var height: String = "",
                       var width: String = "")