package com.kipacraft.riset.realmriset

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.hwangjr.rxbus.RxBus
import com.hwangjr.rxbus.annotation.Subscribe
import com.izzyparcel.android.courier.event.AddPalletEvent
import com.kipacraft.riset.realmriset.adapter.DataSourceAdapter
import com.kipacraft.riset.realmriset.event.EditEvent
import com.kipacraft.riset.realmriset.model.PalletModel
import com.kipacraft.riset.realmriset.model.myRealmModel.PalletRealm
import io.realm.Realm
import kotlinx.android.synthetic.main.main_activity_layout.*

class MainActivity : AppCompatActivity() {

    lateinit var mLayoutManager: LinearLayoutManager
    lateinit var realm: Realm
    lateinit var mAdapter: DataSourceAdapter

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity_layout)
        RxBus.get().register(this)
        realm = Realm.getDefaultInstance()
        mLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        setSupportActionBar(myToolbar)
        listView.apply {
            layoutManager = mLayoutManager
            setHasFixedSize(true)
        }

        addBtn.setOnClickListener { RxBus.get().post(AddPalletEvent()) }
        loadData()
    }

    override fun onDestroy() {
        RxBus.get().unregister(this)
        realm.close()
        super.onDestroy()
    }

    fun loadData() {
        val dataSource: ArrayList<PalletModel> = ArrayList()
        realm.where(PalletRealm::class.java).equalTo("isDeleted", false)
                .findAll()
                .apply {
                    forEach { t ->
                        dataSource.add(PalletModel(t.index,
                                t.code, t.length, t.weight, t.id, t.height, t.width))
                    }
                }

        mAdapter = DataSourceAdapter(dataSource)
        listView.adapter = mAdapter
    }

    private fun getCurrentIndex(): Int {
        val size = realm.where(PalletRealm::class.java).count()
        return if (size > 0) {
            val currNumber: Number = realm.where(PalletRealm::class.java).max("index")!!
            currNumber.toInt() + 1
        } else {
            0
        }
    }

    @Subscribe
    fun listenAddEvent(t: AddPalletEvent) {
        val idx = getCurrentIndex()
        realm.executeTransaction {
            it.createObject(PalletRealm::class.java).apply {
                index = idx
                id = ""
                code = ""
                length = ""
                weight = ""
                height = ""
                width = ""
                isDeleted = false
            }
        }

        realm.where(PalletRealm::class.java).equalTo("index", idx)
                .findFirst()?.also {
                    mAdapter.addItem(PalletModel(it.index, it.code, it.length, it.weight, it.id,
                            it.height, it.width))
                }
    }

    @Subscribe
    fun listenEditEvent(t: EditEvent) {
        val eventPosition = t.position
        println("Edit event: v = ${t.value}, i = ${t.index}")
        realm.executeTransaction {
            it.where(PalletRealm::class.java)
                    .equalTo("index", t.index)
                    .findFirst()?.apply {
                        when (t.fieldName) {
                            "code" -> code = t.value
                            "length" -> length = t.value
                            "height" -> height = t.value
                            "weight" -> weight = t.value
                            "width" -> width = t.value
                        }
                        println("Edit event: executed")
                    }
        }

        /*realm.where(PalletRealm::class.java)
                .equalTo("index", t.index)
                .findFirst()?.also {
                    val pm = PalletModel(it.index, it.code, it.length, it.weight, it.id, it.height, it.width)
                    mAdapter.updateItem(pm, eventPosition)
                }*/
    }
}
