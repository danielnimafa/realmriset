package com.izzyparcel.android.courier.event

class DeletePalletEvent {
    var index: Int = 0
    var position: Int = 0
}