package com.kipacraft.riset.realmriset.model.myRealmModel

import io.realm.RealmModel
import io.realm.annotations.RealmClass

@RealmClass
open class PalletRealm : RealmModel {
    var index: Int = 0
    var id: String = ""
    var code: String = ""
    var length: String = ""
    var weight: String = ""
    var height: String = ""
    var width: String = ""
    var isDeleted: Boolean = false
}