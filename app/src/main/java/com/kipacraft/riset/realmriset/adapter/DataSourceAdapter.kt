package com.kipacraft.riset.realmriset.adapter

import android.support.v7.widget.AppCompatEditText
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import com.hwangjr.rxbus.RxBus
import com.kipacraft.riset.realmriset.R
import com.kipacraft.riset.realmriset.event.EditEvent
import com.kipacraft.riset.realmriset.model.PalletModel

class DataSourceAdapter(val dataSource: ArrayList<PalletModel>) : RecyclerView.Adapter<DataSourceAdapter.ItemHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_item, parent, false)
        return ItemHolder(view, dataSource)
    }

    override fun getItemCount(): Int = dataSource.size

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        holder.apply {
            val item = dataSource[adapterPosition]
            edCode.setText(item.code, TextView.BufferType.EDITABLE)
            handleTextChanged(item, position)
        }
    }

    fun addItem(pickup: PalletModel) {
        println("pallet adapter: add item = $pickup")
        dataSource.add(pickup)
        val position = itemCount - 1
        notifyItemInserted(position)
        notifyItemRangeChanged(position, itemCount)
        println("pallet adapter: current size $itemCount")
    }

    fun removeItem(position: Int) {
        println("pallet adapter: deleted pos: $position")
        if (position < 0 || position >= itemCount) {
            return
        }
        dataSource.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, itemCount)
        println("pallet adapter: current size $itemCount")
    }

    fun updateItem(pickup: PalletModel, position: Int) {
        println("pallet adapter: updated: $pickup")
        dataSource[position] = pickup
        notifyItemChanged(position)
        notifyItemRangeChanged(position, itemCount)
        println("pallet adapter: current size $itemCount")
    }

    class ItemHolder(v: View, val dataSource: ArrayList<PalletModel>) : RecyclerView.ViewHolder(v) {

        val edCode: AppCompatEditText = v.findViewById(R.id.edCode)
        val edWidth: EditText = v.findViewById(R.id.edWidth)
        val edHeight: EditText = v.findViewById(R.id.edHeight)
        val edLength: EditText = v.findViewById(R.id.edLength)
        val edWeight: EditText = v.findViewById(R.id.edWeight)
        val scanBtn: ImageButton = v.findViewById(R.id.scanBtn)
        val deleteBtn: ImageButton = v.findViewById(R.id.deleteBtn)
        val bottomLine: View = v.findViewById(R.id.bottom_line)
        var codeTextWatcher: CustomEditTextWatcher? = null

        init {
            edCode.addTextChangedListener(object : TextWatcher {

                override fun afterTextChanged(s: Editable?) {
                    val value = s?.toString() ?: ""
                    if (value.trim().isNotEmpty()) {
                        with(dataSource[adapterPosition]) {
                            code = value
                            RxBus.get().post(EditEvent().also {
                                it.index = index
                                it.fieldName = "code"
                                it.value = s?.toString() ?: "zonk"
                                it.position = adapterPosition
                            })
                        }
                    }
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                }
            })
            println("ItemHolder onCreate holder")
        }

        fun handleTextChanged(item: PalletModel, pos: Int) {


        }
    }

    class CustomEditTextWatcher(val dataSource: ArrayList<PalletModel>, val fieldName: String) : TextWatcher {

        var position: Int = 0

        fun updatePosition(position: Int) {
            this.position = position
        }

        override fun afterTextChanged(s: Editable?) {
            val value = s?.toString() ?: ""
            if (value.trim().isNotEmpty()) {
                with(dataSource[position]) {
                    RxBus.get().post(EditEvent().also {
                        it.index = index
                        it.fieldName = fieldName
                        it.value = s?.toString() ?: "zonk"
                        it.position = position
                    })
                }
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }
    }
}